//package com.idlecampus;
//
//import java.util.List;
//
//import org.jivesoftware.smack.Chat;
//import org.jivesoftware.smack.MessageListener;
//import org.jivesoftware.smack.PacketInterceptor;
//import org.jivesoftware.smack.PacketListener;
//import org.jivesoftware.smack.XMPPException;
//import org.jivesoftware.smack.packet.Message;
//import org.jivesoftware.smack.packet.Packet;
//import org.jivesoftware.smack.provider.ProviderManager;
//import org.jivesoftware.smackx.pubsub.Item;
//import org.jivesoftware.smackx.pubsub.LeafNode;
//import org.jivesoftware.smackx.pubsub.PubSubManager;
//import org.jivesoftware.smackx.pubsub.Subscription;
//
//import com.idlecampus.IQParser;
//
//import android.os.Bundle;
//import android.app.Activity;
//import android.content.Intent;
//import android.view.Menu;
//import android.view.View;
//
//public class ConfigureStreamsScreen extends Activity{
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.activity_configure_streams_screen);
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.activity_configure_streams_screen,
//				menu);
//		return true;
//	}
//
//	public void create(View view) {
//		Intent intent = new Intent(this, CreateNodeScreen.class);
//		startActivity(intent);
//	}
//
//	public void subscribe(View view) {
//		Intent intent = new Intent(this, SubscribeNodeScreen.class);
//		startActivity(intent);
//	}
//
//	public void streams(View view) {
//		XMPPUtilities utils = XMPPUtilities.getInstance();
//		try {
//			// Create a pubsub manager using an existing Connection
//
//			 ProviderManager pr = ProviderManager.getInstance();
//		     pr.addIQProvider("pubsub", "http://jabber.org/protocol/pubsub", new IQParser());
//
//		 	PubSubManager p = new PubSubManager(utils.connection);
//		 p.getSubscriptions();
//		} catch (XMPPException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 	//  xmpp.login(sname, spass);
//
//		 	String nodes = IQParser.nodes;
//		 Intent intent = new Intent(this, StreamsScreen.class);
//		 intent.putExtra("NODES", nodes);
//		 startActivity(intent);
//	}
//
//
//}
