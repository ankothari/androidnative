package com.idlecampus;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
public class SplashScreen extends Activity {

    Intent i;
    public XMPP xmpp = null;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // used to know if the back button was pressed in the splash screen activity and avoid opening the next activity
    private boolean mIsBackButtonPressed;
    private static final int SPLASH_DURATION = 2000; // 2 seconds
    // Connection detector class
    ConnectionDetector cd;
    XMPPUtilities utils = XMPPUtilities.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("idlecampus", "oncreate");
        super.onCreate(savedInstanceState);


        // creating connection detector class instance
        cd = new ConnectionDetector(getApplicationContext());


        requestWindowFeature(getWindow().FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.splashscreen);
        utils.connection.DEBUG_ENABLED = true;


        // Restore preferences
        SharedPreferences settings = getSharedPreferences("XMPP", 0);
        String name = settings.getString("name", "");
        String password = settings.getString("password", "");
        Log.i("namepassword", name + " " + password);

        Log.i("idlecampus", utils.connection.isAuthenticated() + "");


        Log.i("idlecampus", "oncreate");
        String device_identifier = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedPreferences.Editor editor = settings.edit();
        editor.putString("device_identifier", device_identifier);
        // Commit the edits!
        editor.commit();


        super.onCreate(savedInstanceState);
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        final String regId = GCMRegistrar.getRegistrationId(this);
//        GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
        if (regId.equals(""))
        {
            GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
        } else {
            Log.v("idlecampus", "Already registered");
        }


        String group = settings.getString("group_name", "");
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (utils.connection.isAuthenticated()) {
                Intent intent = new Intent(this, MyGroup.class);
                intent.putExtra("group", group);
                startActivity(intent);
                finish();
            } else if (!name.equalsIgnoreCase("") && !password.equalsIgnoreCase("")) {

//                 String code = settings.getString("code","");
//                if(code.equalsIgnoreCase("2")){
                    Intent intent = new Intent(this, MyGroup.class);
                    intent.putExtra("group", group);
                    startActivity(intent);
                    finish();
//                }
//                else if(code.equalsIgnoreCase("1")){
//                    Intent i = new Intent(this, NewMessage.class);
//                    i.putExtra("group", group);
//
//                   startActivity(i);
//                   finish();
//                }




                login(name, password, "autologin");
//                finish();
            }
            else {
                Handler handler = new Handler();

                // run a thread after 2 seconds to start the home screen
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        // make sure we close the splash screen so the user won't come back when it presses back key

                        finish();

                        if (!mIsBackButtonPressed) {
                            // start the home screen if the back button wasn't pressed already
                            Intent intent = new Intent(SplashScreen.this, HomeScreen
                                    .class);
                            SplashScreen.this.startActivity(intent);
                        }

                    }

                }, SPLASH_DURATION); // time in milliseconds (1 second = 1000 milliseconds) until the run() method will be called


            }
        }
        else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }


//            startService(new Intent(getBaseContext(), MyService.class));

    }

    public void forgotPassword(View view) {
        Intent intent = new Intent(this, ForgotPassword.class);
        startActivity(intent);
    }



    @Override
    public void onBackPressed() {

        // set the flag to true so the next activity won't start up
        mIsBackButtonPressed = true;
        super.onBackPressed();

    }

    public void login(View view) {

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            EditText editTextName = (EditText) findViewById(R.id.loginusername);
            String name = editTextName.getText().toString();
            if (name.matches("")) {
                Toast.makeText(this, "You did not enter a username", Toast.LENGTH_SHORT).show();
                return;
            }
            EditText editTextPassword = (EditText) findViewById(R.id.loginpassword);
            String password = editTextPassword.getText().toString();
            if (password.matches("")) {
                Toast.makeText(this, "You did not enter a password", Toast.LENGTH_SHORT).show();
                return;
            }
            login(name, password, "login");
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }


    }

    public void login(String name, String password, String status) {
        XMPP xmpp = new XMPP();
        xmpp.isResumed = true;
        System.out.println(utils.connection.isConnected());

        if (utils.connection.isAuthenticated()) {


            Log.i("idlecampus", "already logged in");

        } else {

            xmpp.registerandlogin(this, utils, name, password, "", "", status);
        }

    }


    public void register(View view) {
        Intent intent = new Intent(this, RegisterScreen.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_home_screen, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }



    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
//        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


}
