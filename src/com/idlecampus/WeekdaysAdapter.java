package com.idlecampus;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class WeekdaysAdapter extends BaseAdapter {
	public String[] streams = null;
public WeekdaysAdapter(){
	
}
	public int getCount() {
		// TODO Auto-generated method stub
		return streams.length;
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return streams[arg0];
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View view, ViewGroup parent) {
		if(view == null){
			LayoutInflater inflator = LayoutInflater.from(parent.getContext());
			view = inflator.inflate(R.layout.stream_list_item,parent,false);
		}
		String stream = streams[position];
		TextView textView = (TextView)view.findViewById(R.id.time_view);
		textView.setText(stream);



		return view;
	}

}
