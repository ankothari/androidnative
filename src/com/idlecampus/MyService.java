package com.idlecampus;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.pubsub.Affiliation;
import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.PubSubManager;
import org.jivesoftware.smackx.pubsub.Subscription;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 29/6/13
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyService extends Service implements MessageListener, PacketListener {

    private final IBinder binder = new MyBinder();


    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
// We want this service to continue running until it is explicitly // stopped, so return sticky.
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show(); Object[] objUrls = (Object[]) intent.getExtras().get("URLs"); URL[] urls = new URL[objUrls.length];
//        for (int i=0; i<objUrls.length-1; i++) {
//            urls[i] = (URL) objUrls[i]; }
//        DownloadWebPageTask task = new DownloadWebPageTask();
//        task.execute(new Object[]{utils, "", "","", "", ""});
//        return START_STICKY;
return 0;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this,"Service Destroyed", Toast.LENGTH_LONG).show();
    }


    static XMPPUtilities utils = null;

    private Activity currentActivity = null;

	protected URL[] urls;


    public JSONObject getGroupName(String group) {
        String page = null;

        JSONObject object = new JSONObject();
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://idlecampus.com/api/groups/" + group);

        // httpGet.addHeader("Content-type","application/json");


        ResponseHandler<String> resHandler = new BasicResponseHandler();

        try {
            page = httpClient.execute(httpGet, resHandler);
        } catch (ClientProtocolException e) {

            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block

        }
        Log.e("response", page);
        try {


            JSONObject jsonObject = new JSONObject(page);


            try {
                object.put("group_name", jsonObject.getString("group_name"));
                object.put("group_code", jsonObject.getString("group_code"));
//                object.put("code", "2");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println(object);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;

    }

    public static void subscribeNode(String node) {

        XMPPUtilities utils = XMPPUtilities.getInstance();
        PubSubManager mgr = utils.mgr;
        LeafNode leaf;

        try {
            Log.i("idlecampusgroup", node);
            leaf = (LeafNode) mgr.getNode(node);
            leaf.subscribe(utils.connection.getUser());
        } catch (XMPPException e) {
            // TODO Auto-generated catch block

            Log.i("idlecampus", "group not found");

            e.printStackTrace();
        }
    }

    private void connect() {
        XMPPUtilities utils = XMPPUtilities.getInstance();
        utils.connection.DEBUG_ENABLED = true;
        try {

            utils.connection.connect();
            System.out.println(utils.connection.isConnected());
            Log.i("idlecampus", "packet listener");
            utils.connection.addPacketListener(this, null);
        } catch (XMPPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void gotRoster() {
        Roster roster = utils.connection.getRoster();
        roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
        roster.addRosterListener(new RosterListener() {
            public void entriesDeleted(Collection<String> addresses) {
                System.out.println("deleted");
            }

            public void entriesUpdated(Collection<String> addresses) {
                System.out.println("updated");
            }

            public void presenceChanged(Presence presence) {
                System.out.println(presence.getType());
                System.out.println("Presence changed: " + presence.getFrom()
                        + " " + presence);
            }

            public void entriesAdded(Collection<String> arg0) {
                System.out.println("added");

            }
        });
        Collection<RosterEntry> entries = roster.getEntries();

        System.out.println("\n\n" + entries.size() + " buddy(ies):");
        for (RosterEntry r : entries) {
            System.out.println(r.getUser());
        }


        Map copyFrom = new HashMap();
        copyFrom.put("roster", entries);
//        c.success(new JSONObject(copyFrom));
    }

    private void login(String name, String password) {
        try {
            Log.i("", "inside login");
            utils = XMPPUtilities.getInstance();
            System.out.println(utils.connection.isConnected());
            if (!utils.connection.isConnected())
                utils.connection.connect();


            if (utils.connection.isAuthenticated()) {


                Log.i("idlecampus", "already logged in");

            } else {

                utils.connection.login(name, password);

                // See if you are authenticated
                System.out.println(utils.connection.isAuthenticated());
                if (utils.connection.isAuthenticated()) {
                    Log.i("idlecampus", "logging in");

//				this.gotRoster();
                }


            }
        } catch (XMPPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void processPacket(Packet arg0) {
        Context context = currentActivity.getApplicationContext();
        Intent notificationIntent = new Intent(context,
                HomeScreen.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
//                notificationIntent, 0);


        // TODO Auto-generated method stub
        System.out.println("xmpppacket " + arg0.toXML());
        StringReader sr = new StringReader(arg0.toXML());
        InputSource is = new InputSource(sr);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();


        try {

            DocumentBuilder builder = dbf.newDocumentBuilder();


            Document doc = builder.parse(is);
            doc.getDocumentElement().normalize();
            System.out.println("Root element of the doc is " +
                    doc.getDocumentElement().getNodeName());
            NodeList listOfPersons = doc.getElementsByTagName("item");
            System.out.println("listofpersons" +
                    listOfPersons.getLength());
            Node item = ((Node) listOfPersons.item(0));
            System.out.println("textFNList" +
                    item);

            NodeList textFNList = item.getChildNodes();
            String message =
                    ((Node) textFNList.item(0)).getNodeValue().trim();


            String message_to_send = message.substring(4, message.length() - 4);

            String contentTitle = message_to_send;
            String contentText = message_to_send;

            int icon = 0;// = R.drawable.ic_stat_gcm;
            long when = System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new Notification(R.drawable.ic_launcher, message, when);
//            notification.vibrate = new long[] { 100, 250, 100, 500};
            String title = context.getString(R.string.app_name);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notification.sound = alarmSound;
//            Notification noti = new Notification.Builder(context)
//                    .setContentTitle(message_to_send)
//                    .setContentText("Message").setSmallIcon(R.drawable.icon)
//                    .setContentIntent(contentIntent)
//                    .setSound(alarmSound)
//                    .build();

            notificationIntent = new Intent(context, HomeScreen.class);
            // set intent so it does not start a new activity
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent =
                    PendingIntent.getActivity(context, 0, notificationIntent, 0);
            notification.setLatestEventInfo(context, title, message, intent);
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, notification);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
//            c.error(e.getMessage());
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
//            c.error(e.getMessage());
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
//            c.error(e.getMessage());
        }


    }


    @Override
    public void processMessage(Chat arg0, Message arg1) {
        // TODO Auto-generated method stub
        System.out.println("xmppchat " + arg1);

    }


    private class DownloadWebPageTask extends AsyncTask<Object, Void, Void> {

        private ProgressDialog waitingDialog;

        @Override
        protected Void doInBackground(Object... urls) {
            String response = "";
            SharedPreferences settings = currentActivity.getApplicationContext().getSharedPreferences("XMPP", 0);
            Map<String, String> params = new HashMap<String, String>();
            SharedPreferences.Editor editor = settings.edit();

            XMPPUtilities utils = (XMPPUtilities) urls[0];
            String name = (String) urls[1];
            String password = (String) urls[2];
            String group = (String) urls[4];
            String status = (String) urls[5];
            String email = (String) urls[3];
            Log.i("idlecampus", "" + status);

            Log.i("idlecampus", "" + name);

            Log.i("idlecampus", "" + password);

            Log.i("idlecampus", "" + utils);


            Log.i("idlecampus", "" + utils.connection.isConnected());
            AccountManager am = new AccountManager(utils.connection);


            if (status.equalsIgnoreCase("register")) {

                try {

//                    utils.connection.connect();


                    am.createAccount(name, password);



                    String device_identifier = settings.getString("device_identifier", "");
                    params.put("device_identifier", device_identifier);
                    params.put("jabber_id", name + "@idlecampus.com");
                    params.put("email", email);
                    params.put("name",name);
                    params.put("password",password);
                    editor.putString("name", name);
                    editor.putString("password", password);
                    editor.putString("email", email);
                    // Commit the edits!
                    editor.commit();



                    try {
                        Log.i("idlecampus", "sending");
                        ServerUtilities.post("http://idlecampus.com/api/users", params);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        Log.i("idlecampus", "register");
                        e.printStackTrace();
                    }
                    Log.i("", "inside register");

                    System.out.println(utils.connection.isConnected());


                } catch (XMPPException e) {


                    e.printStackTrace();
                }
            }


            try {


                Log.i("idlecampus", "inside login");


                if (!utils.connection.isConnected()) {
                    utils.connection.connect();
                    Log.i("idlecampus", "packet listener");
                    utils.connection.addPacketListener(MyService.this, null);
                }

                if (utils.connection.isAuthenticated()) {


                    Log.i("idlecampus", "already logged in");
//                    temp.get("login").success();
                } else {

                    utils.connection.login(name, password);

                    // See if you are authenticated
                    System.out.println(utils.connection.isAuthenticated());
                    if (utils.connection.isAuthenticated()) {
                        Log.i("idlecampus", "logging in");

                        if (status.equalsIgnoreCase("register")) {
                            Log.i("idlecampus", "joining group");

                            JSONObject jsonObject = getGroupName(group);
                            if (jsonObject != null) {
                                XMPP.subscribeNode(group);
                                editor.putString("code","2");
                                try {
                                    editor.putString("group_code",jsonObject.getString("group_name"));
                                    editor.putString("group_name",jsonObject.getString("group_code"));
                                    editor.commit();
                                } catch (JSONException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }

                                try {
                                    getTimetable(group);

                                    showGroup(utils, jsonObject.getString("group_name"));
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            } else {
                                Context context = currentActivity.getApplicationContext();
                                CharSequence text = "Group not found.!";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }


                        } else if (status.equalsIgnoreCase("login")) {
                            //return here with a status of 1.

                            //get group name and then show timetable for the group.

                            try {

                                List<Subscription> subscriptions = utils.mgr.getSubscriptions();
                                for (int i = 0; i < subscriptions.size(); i++) {
                                    Log.i("idlecampussubscriptions", subscriptions.get(i).getNode());
                                }


                                // Get the affiliations for the users bare JID
                                List<Affiliation> affiliations = utils.mgr.getAffiliations();

                                for (int i = 0; i < affiliations.size(); i++) {
                                    Log.i("idlecampusaffiliations", affiliations.get(i).getNode());
                                }

                                if (subscriptions.size() > 0) {
                                    JSONObject object = getGroupName(subscriptions.get(0).getNode());
                                    try {
                                        getTimetable(object.getString("group_code"));

                                        showGroup(utils, object.getString("group_name"));
                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                } else if (affiliations.size() > 0) {
                                    JSONObject object = getGroupName(affiliations.get(0).getNode());
                                    Intent i = new Intent(currentActivity.getBaseContext(), NewMessage.class);
                                    i.putExtra("group", group);

                                    currentActivity.startActivity(i);
                                }

                            } catch (XMPPException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }




                            Log.i("idlecampus", "packet listener");
                            utils.connection.addPacketListener(MyService.this, null);
//                            temp.get("login1").success("1");
                        } else
                            Log.i("idlecampus", "packet listener");
                        utils.connection.addPacketListener(MyService.this, null);
                        Log.i("idlecampus", "sending back login1 with status 2");
//                        temp.get("login").success("2");


                    }


                }


            } catch (XMPPException e) {
                XMPPUtilities.xmppUtils = null;
                editor.putString("name", "");
                editor.putString("password", "");
                editor.commit();
                currentActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Context context = currentActivity.getApplicationContext();
                        CharSequence text = "Invalid Credentials.!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                });

                e.printStackTrace();
            }


            return null;
        }

        public JSONObject getGroupName(String group) {
            String page = null;

            JSONObject object = new JSONObject();
            DefaultHttpClient httpClient = new DefaultHttpClient();

            HttpGet httpGet = new HttpGet("http://idlecampus.com/api/groups/" + group);
            // httpGet.addHeader("Content-type","application/json");


            ResponseHandler<String> resHandler = new BasicResponseHandler();

            try {
                page = httpClient.execute(httpGet, resHandler);
            } catch (ClientProtocolException e) {

                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block

            }
            Log.e("response", page);
            if (page.equalsIgnoreCase("group not found")) {
                return null;
            } else {
                try {


                    JSONObject jsonObject = new JSONObject(page);


                    try {
                        object.put("group_name", jsonObject.getString("group_name"));
                        object.put("group_code", jsonObject.getString("group_code"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    System.out.println(object);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return object;

        }

        private void showGroup(XMPPUtilities utils, String group) {
            Log.i("idlecampus", "packet listener");
            utils.connection.addPacketListener(MyService.this, null);
            Intent i = new Intent(currentActivity.getBaseContext(), MyGroup.class);
            i.putExtra("group", group);

            currentActivity.startActivity(i);


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            final Activity t = cordova.getActivity();

            waitingDialog = ProgressDialog.show(currentActivity, "", "Initializing...", true);
            Log.d("WaitingDialog", "Initializing...");
            Log.d("WaitingDialog", "Dialog shown, waiting hide command");


        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            waitingDialog.dismiss();
        }

        public void getTimetable(String group) {
            Timetable timetable = new Timetable();
            timetable.currentActivity = currentActivity;
            timetable.notification("http://idlecampus.com/group/"+group+"/timetable.json");

        }
    }

    public void registerandlogin(Activity act, XMPPUtilities utils, String name, String password,String email, String group, String status) {
        currentActivity = act;
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(new Object[]{utils, name, password,email, group, status});

    }

    public class MyBinder extends Binder { MyService getService() {
        return MyService.this; }
    }


}

