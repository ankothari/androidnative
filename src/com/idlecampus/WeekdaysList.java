package com.idlecampus;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 28/6/13
 * Time: 1:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class WeekdaysList extends Activity implements AdapterView.OnItemClickListener {
    IntentFilter intentFilter;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] weekdaysArray = null;


        //---intent to filter for file downloaded intent---
        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);


        setContentView(R.layout.activity_weekdays_screen);

        SharedPreferences settings = getApplicationContext().getSharedPreferences("XMPP", 0);
        String weekdaysJson = settings.getString("weekdays", "");
        Gson gson = new Gson();
        List<String> weekdaysList = null;
        try {
            JSONObject jsonObject = new JSONObject(weekdaysJson);
            JSONArray weekdays = jsonObject.getJSONArray("values");
            weekdaysList = new ArrayList<String>();

            for(int len = 0;len < weekdays.length();len++){
                weekdaysList.add(weekdays.getString(len));
            }
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        ListView listView = (ListView) findViewById(R.id.listView1);
        if (weekdaysList != null)  {
            weekdaysArray = new String[weekdaysList.size()];
            for(int count = 0 ; count < weekdaysList.size() ; count++ ){
                weekdaysArray[count] = weekdaysList.get(count);
            }
            StreamsAdapter streamsAdapter = new StreamsAdapter();

            streamsAdapter.streams = weekdaysArray;
            listView.setAdapter(streamsAdapter);
            listView.setOnItemClickListener(this);
        }





    }
    @Override
    public void onResume() {
        super.onResume();

        //---intent to filter for file downloaded intent---
        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(intentReceiver);
    }

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
		 RelativeLayout ll = (RelativeLayout) view;
	        TextView tv = (TextView) ll.findViewById(R.id.group_title);
	        final String weekday = tv.getText().toString();
	       
	            Intent i = new Intent(getBaseContext(), WeekdayTimetableList.class);
	            i.putExtra("weekday", weekday);
	            // find out the string value and set the contents of the next page accordingly.
	            startActivity(i);
	        
		
	}
    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {



            String message_to_send = intent.getStringExtra("message");


            String contentTitle = message_to_send;
            String contentText = message_to_send;

            int icon = 0;// = R.drawable.ic_stat_gcm;
            long when = System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new Notification(R.drawable.icon, message_to_send, when);
//            notification.vibrate = new long[] { 100, 250, 100, 500};
            String title = context.getString(R.string.app_name);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notification.sound = alarmSound;
            Intent notificationIntent = new Intent(context,
                    GroupMessagesActivity.class);
            notificationIntent.putExtra("messageReceived", message_to_send);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pIntent =
                    PendingIntent.getActivity(context, 0, notificationIntent, 0);
            notification.setLatestEventInfo(context, title, message_to_send, pIntent);
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, notification);

        }
    };
}