package com.idlecampus;

import java.util.ArrayList;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class StreamsAdapter extends BaseAdapter {
	public String[] streams = null;
public StreamsAdapter(){
	
}
	public int getCount() {
		// TODO Auto-generated method stub
		return streams.length;
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return streams[arg0];
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View view, ViewGroup parent) {
		if(view == null){
			LayoutInflater inflator = LayoutInflater.from(parent.getContext());
			view = inflator.inflate(R.layout.group_item,parent,false);
		}
		String stream = streams[position];
		TextView textView = (TextView)view.findViewById(R.id.group_title);
		textView.setText(stream);
        ImageView img = null;
        TextView mails = (TextView)view.findViewById(R.id.mails);
        mails.setText("");
        img = (ImageView)view.findViewById(R.id.group_image);
        if(stream.equalsIgnoreCase("Timetable"))
              img.setImageResource(R.drawable.timetable);
        if(stream.equalsIgnoreCase("Messages")) {
              img.setImageResource(R.drawable.mail);
            mails.setText("");
        }

		return view;
	}

}
