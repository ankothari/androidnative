package com.idlecampus;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 25/7/13
 * Time: 9:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class Friends extends Activity implements AdapterView.OnItemClickListener {
    private ListView list;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        XMPPUtilities utils = XMPPUtilities.getInstance();
        // get roster
        setContentView(R.layout.activity_friends_screen);
        String[] friendsArray = null;
        Roster roster = utils.connection.getRoster();
        Collection<RosterEntry> entries = roster.getEntries();

        System.out.println("\n\n" + entries.size() + " buddy(ies):");
        for (RosterEntry r : entries) {
            System.out.println(r.getUser());
        }


        Map copyFrom = new HashMap();
        copyFrom.put("roster", entries);

        List<String> friendsList = null;


            friendsList = new ArrayList<String>();
            for (RosterEntry r : entries) {
                System.out.println(r.getUser());
                friendsList.add(r.getUser());
            }



        list = (ListView) findViewById(R.id.listView1);

        List<String> weekdaylist = friendsList;
        friendsArray = new String[weekdaylist.size()];

        StreamsAdapter streamsAdapter = new StreamsAdapter();
        for(int count=0;count<friendsList.size();count++){
            friendsArray[count] = friendsList.get(count);
        }
        SharedPreferences settings = getApplicationContext().getSharedPreferences("XMPP", 0);
        SharedPreferences.Editor editor = settings.edit();
         if(friendsList.size() > 0){

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < friendsArray.length; i++) {
            sb.append(friendsArray[i]).append(",");
        }
        editor.putString("friends", sb.toString());

        editor.commit();
         }
        else{
             String friends = settings.getString("friends","");
             friendsArray = friends.split(",");
         }


        streamsAdapter.streams = friendsArray;
        list.setAdapter(streamsAdapter);
        list.setOnItemClickListener(this);
    }
    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
        RelativeLayout ll = (RelativeLayout) view;
        TextView tv = (TextView) ll.findViewById(R.id.group_title);
        final String item = tv.getText().toString();

        Intent i = new Intent(getBaseContext(), BingoActivity.class);

        i.putExtra("userPlayingBingoWith",item);

        startActivity(i);


    }
}