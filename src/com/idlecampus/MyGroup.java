package com.idlecampus;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 27/6/13
 * Time: 3:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class MyGroup extends Activity implements OnItemClickListener {
    private ListView list;
    private String group;
    IntentFilter intentFilter;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //---intent to filter for file downloaded intent---
        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);
        String list_array[] = {"Timetable", "Messages","Notes"};





        setContentView(R.layout.activity_group_screen);

        Intent intent = getIntent();
        group = intent.getStringExtra("group");

//        TextView text = (TextView) findViewById(R.id.group_title);
//        text.setText(group);

        //ImageView Setup
        ImageView imageView = new ImageView(this);
        //setting image resource
        imageView.setImageResource(R.drawable.mailicon);
//        //setting image position
//        imageView.setLayoutParams(new LayoutParams(
//                LayoutParams.MATCH_PARENT,
//                LayoutParams.WRAP_CONTENT));
//
//        //adding view to layout
//        linearLayout.addView(imageView);
//        //make visible to program
//        setContentView(linearLayout);
        list = (ListView) findViewById(R.id.listView1);

        StreamsAdapter streamsAdapter = new StreamsAdapter();
        streamsAdapter.streams = list_array;
        list.setAdapter(streamsAdapter);
        list.setOnItemClickListener(this);




    }
    @Override
    public void onResume() {
        super.onResume();

        //---intent to filter for file downloaded intent---
        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(intentReceiver);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
        RelativeLayout ll = (RelativeLayout) view;
        TextView tv = (TextView) ll.findViewById(R.id.group_title);
        final String item = tv.getText().toString();
        if(item.equalsIgnoreCase("timetable")){
            Intent i = new Intent(getBaseContext(), WeekdaysList.class);

            startActivity(i);


        }
        if(item.equalsIgnoreCase("messages") || item.equalsIgnoreCase("New Message")){

            TextView text = (TextView)findViewById(R.id.mails);

            text.setText("");

            Intent i = new Intent(getBaseContext(), GroupMessagesActivity.class);

            startActivity(i);
        }
        if(item.equalsIgnoreCase("Notes")){


            Intent i = new Intent(getBaseContext(), NotesActivity.class);

//            i.putExtra("files", page);

            startActivity(i);
        }


    }

    public void onBackPressed(){
        XMPPUtilities utils  = XMPPUtilities.getInstance();
        utils.connection.disconnect();
        finish();
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        Toast.makeText(getBaseContext(), "back",Toast.LENGTH_SHORT).show();
    }

    // Initiating Menu XML file (menu.xml)
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.settings, menu);
        return true;
    }

    /**
     * Event Handling for Individual menu item selected
     * Identify single menu item by it's id
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.signout:
                SharedPreferences settings = getSharedPreferences("XMPP", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("name", "");
                editor.putString("password","");
                // Commit the edits!
                editor.commit();

                XMPPUtilities utils  = XMPPUtilities.getInstance();
                utils.connection.disconnect();
                finish();
                Intent intent = new Intent(this, HomeScreen.class);
                startActivity(intent);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String str = (String) list.getItemAtPosition(1);
            View view = list.getChildAt(1);
            TextView text = (TextView)view.findViewById(R.id.mails);
//            String message_to_send = text.getText().toString();
            String strNumberOfMailsReceived = text.getText().toString();
            int intNumberOfMailsReceived = 0;
            if(!strNumberOfMailsReceived.equalsIgnoreCase(""))  {
            intNumberOfMailsReceived = Integer.parseInt(strNumberOfMailsReceived);
            intNumberOfMailsReceived++;
            text.setText(intNumberOfMailsReceived+"");
            }
            else{
                text.setText("1");
            }
            String message_to_send = intent.getStringExtra("message");


            String contentTitle = message_to_send;
            String contentText = message_to_send;

            int icon = 0;// = R.drawable.ic_stat_gcm;
            long when = System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new Notification(R.drawable.icon, message_to_send, when);
//            notification.vibrate = new long[] { 100, 250, 100, 500};
            String title = context.getString(R.string.app_name);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notification.sound = alarmSound;
            Intent notificationIntent = new Intent(context,
                    GroupMessagesActivity.class);
			notificationIntent.putExtra("messageReceived", message_to_send);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pIntent =
                    PendingIntent.getActivity(context, 0, notificationIntent, 0);
            notification.setLatestEventInfo(context, title, message_to_send, pIntent);
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, notification);

        }
    };
}