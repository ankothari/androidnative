package com.idlecampus;

import java.util.HashMap;
import java.util.List;

public class MapWrapper {
    private HashMap<String, List<String>> myMap;
    // getter and setter for 'myMap'

	public HashMap<String, List<String>> getMyMap() {
		return myMap;
	}

	public void setMyMap(HashMap<String, List<String>> myMap) {
		this.myMap = myMap;
	}
}