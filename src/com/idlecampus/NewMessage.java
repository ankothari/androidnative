package com.idlecampus;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.PayloadItem;
import org.jivesoftware.smackx.pubsub.SimplePayload;
import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 28/6/13
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class NewMessage extends Activity {
    public  ListView listView = null;
    List<String> sentMessages = null;
	private SentGroupMessagesAdapter sentGroupMessagesAdapter;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sentMessages = new ArrayList<String>();

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String savedMessages = appSharedPrefs.getString("SentMessages", "");
        JSONArray jsonSavedSentMessages = gson.fromJson(savedMessages, JSONArray.class);

        if (jsonSavedSentMessages == null)
            jsonSavedSentMessages = new JSONArray();
        for (int i=0; i<jsonSavedSentMessages.length(); i++) {
            try {
				sentMessages.add( jsonSavedSentMessages.getString(i) );
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        setContentView(R.layout.activity_new_message_screen);

        Intent intent = getIntent();
        String group = intent.getStringExtra("group");

        TextView text = (TextView) findViewById(R.id.textView1);
        text.setText(group);



        listView = (ListView) findViewById(R.id.sentGroupMessagesList);
        sentGroupMessagesAdapter = new SentGroupMessagesAdapter();

        SentGroupMessagesAdapter.sentMessages = sentMessages;
        listView.setAdapter(sentGroupMessagesAdapter);
    }



    public void onBackPressed(){
        XMPPUtilities utils  = XMPPUtilities.getInstance();
        utils.connection.disconnect();
        finish();
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        Toast.makeText(getBaseContext(), "back",Toast.LENGTH_SHORT).show();
    }

    public void publishMessage(View view){



        Log.i("idlecampus","publish_message");
        XMPPUtilities utils = XMPPUtilities.getInstance();
        EditText editTextMessage = (EditText) findViewById(R.id.newmessage);
        
        String group = "6GT3XJ";
        final String message = editTextMessage.getText().toString();
        editTextMessage.setText("");
        // Get the node
        LeafNode node;
        try {
            node = (LeafNode) utils.mgr.getNode(group);
            node.send(new PayloadItem<SimplePayload>( ""+System.currentTimeMillis(), new SimplePayload("", "stage:pubsub:simple", "<value>"+message+"</value>")));


            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String savedMessages = appSharedPrefs.getString("SentMessages", "");
            JSONArray jsonSavedMessages = gson.fromJson(savedMessages, JSONArray.class);
            if (jsonSavedMessages == null)
                jsonSavedMessages = new JSONArray();






                jsonSavedMessages.put(message);



                String json = gson.toJson(jsonSavedMessages);
                prefsEditor.putString("SentMessages", json);
                prefsEditor.commit();
                System.out.println(json);



            sentGroupMessagesAdapter.sentMessages.add(message);

            sentGroupMessagesAdapter.notifyDataSetChanged();




        } catch (XMPPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void clearMessages(View view){

        sentGroupMessagesAdapter.sentMessages.clear();

        sentGroupMessagesAdapter.notifyDataSetChanged();


        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String savedMessages = appSharedPrefs.getString("SentMessages", "");
        JSONArray jsonSavedMessages = gson.fromJson(savedMessages, JSONArray.class);

        jsonSavedMessages = new JSONArray();




        String json = gson.toJson(jsonSavedMessages);
        prefsEditor.putString("Messages", json);
        prefsEditor.commit();
        System.out.println(json);
    }
}