package com.idlecampus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.google.gson.Gson;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NotesActivity extends Activity  implements AdapterView.OnItemClickListener {
    List<String> files = null;

    HashMap hm = new HashMap();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = this.getSharedPreferences("XMPP", 0);
        SharedPreferences.Editor editor = settings.edit();
        String group = settings.getString("group_code", "");
        String url = "http://idlecampus.com/groups/"+group+"/notes";
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadNotesTask().execute(url);
        } else {
            showAlertDialog(this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        setContentView(R.layout.activity_files_screen);



    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {

        RelativeLayout ll = (RelativeLayout) view;
        TextView tv = (TextView) ll.findViewById(R.id.group_title);
        final String file = tv.getText().toString();

        Intent i = new Intent(getBaseContext(), NotesDetailActivity.class);
        JSONObject f = (JSONObject)hm.get(file);
        String link = "";
        String message = "";
        try{
        link = f.getString("url");
        message = f.getString("message");
        }catch(Exception e){
            e.printStackTrace();
        }
        i.putExtra("file", file);

        i.putExtra("link",link);
        i.putExtra("message",message);

        // find out the string value and set the contents of the next page accordingly.
        startActivity(i);


    }
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
//        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    private class DownloadNotesTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = ProgressDialog.show(NotesActivity.this, "", "Fetching Notes...", true);
            pDialog.show();

        }
        @Override
        protected String doInBackground(String... urls) {


            	  System.out.println(urls[0]);
                  String page = null;

                  JSONObject object = new JSONObject();
                  DefaultHttpClient httpClient = new DefaultHttpClient();
                  HttpGet httpGet = new HttpGet(urls[0]);


                  ResponseHandler<String> resHandler = new BasicResponseHandler();
                   files = new ArrayList<String>();
                   try {
                       page = httpClient.execute(httpGet, resHandler);
                      
                   } catch(ClientProtocolException e) {

                       e.printStackTrace();
                   } catch (IOException e) {

                       e.printStackTrace();
                       // TODO Auto-generated catch block

                   }
				return page;

        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try{
                pDialog.dismiss();
            }
            catch(Exception e){
               e.printStackTrace();
            }
        	 try {
                 JSONObject jsonObject = new JSONObject(result);
                 // Save the JSONOvject
                 String s = jsonObject.getString("files");
                 JSONArray json_data1 = new JSONArray(s);

                 for (int i = 0; i < json_data1.length(); i++) {
                     jsonObject = json_data1.getJSONObject(i);

                     String name = jsonObject.getString("name");
                     hm.put(name,jsonObject);
                     files.add(name);


                 }
             }catch (JSONException e) {
            	e.printStackTrace();
             } 

             ListView listView = (ListView) findViewById(R.id.listView1);

             StreamsAdapter streamsAdapter = new StreamsAdapter();
             String[] array = files.toArray(new String[files.size()]);
             streamsAdapter.streams = array  ;
             listView.setAdapter(streamsAdapter);
             listView.setOnItemClickListener(NotesActivity.this);
        }
    }
}
