package com.idlecampus;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.preference.PreferenceManager;
import android.widget.Toast;
import com.google.gson.Gson;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.io.StringReader;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.Roster.SubscriptionMode;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.PayloadItem;
import org.jivesoftware.smackx.pubsub.PubSubManager;
import org.jivesoftware.smackx.pubsub.SimplePayload;
import org.jivesoftware.smackx.pubsub.Subscription;
import org.jivesoftware.smackx.pubsub.Affiliation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import org.apache.cordova.api.CallbackContext;
//import org.apache.cordova.api.CordovaPlugin;
//import org.apache.cordova.api.LOG;
//import org.json.JSONArray;
//import org.json.JSONException;
//import android.app.ProgressDialog;
//import android.app.Activity;
//import android.os.Handler;
//import android.content.Intent;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.sax.Element;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

/**
 * This class echoes a string called from JavaScript.
 */
public class XMPP implements MessageListener, PacketListener {

    static XMPPUtilities utils = null;
    public boolean isResumed = false;
    private Activity currentActivity = null;




    public static void subscribeNode(String node) {

        XMPPUtilities utils = XMPPUtilities.getInstance();
        PubSubManager mgr = utils.mgr;
        LeafNode leaf;

        try {
            Log.i("idlecampusgroup", node);
            leaf = (LeafNode) mgr.getNode(node);
            leaf.subscribe(utils.connection.getUser());
        } catch (XMPPException e) {
            // TODO Auto-generated catch block

            Log.i("idlecampus", "group not found");

            e.printStackTrace();
        }
    }

    private void connect() {
        XMPPUtilities utils = XMPPUtilities.getInstance();
        utils.connection.DEBUG_ENABLED = true;
        try {

            utils.connection.connect();
            System.out.println(utils.connection.isConnected());
            Log.i("idlecampus", "packet listener");
            utils.connection.addPacketListener(this, null);
        } catch (XMPPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void gotRoster() {
        XMPPUtilities utils = XMPPUtilities.getInstance();
        Roster roster = utils.connection.getRoster();
        roster.setSubscriptionMode(SubscriptionMode.accept_all);
        roster.addRosterListener(new RosterListener() {
            public void entriesDeleted(Collection<String> addresses) {
                System.out.println("deleted");
            }

            public void entriesUpdated(Collection<String> addresses) {
                System.out.println("updated");
            }

            public void presenceChanged(Presence presence) {
                System.out.println(presence.getType());
                System.out.println("Presence changed: " + presence.getFrom()
                        + " " + presence);
            }

            public void entriesAdded(Collection<String> arg0) {
                System.out.println("added");

            }
        });
        Collection<RosterEntry> entries = roster.getEntries();

        System.out.println("\n\n" + entries.size() + " buddy(ies):");
        for (RosterEntry r : entries) {
            System.out.println(r.getUser());
        }


        Map copyFrom = new HashMap();
        copyFrom.put("roster", entries);
//        c.success(new JSONObject(copyFrom));
    }

    private void login(String name, String password) {
        try {
            Log.i("", "inside login");
            utils = XMPPUtilities.getInstance();
            System.out.println(utils.connection.isConnected());
            if (!utils.connection.isConnected())
                utils.connection.connect();


            if (utils.connection.isAuthenticated()) {


                Log.i("idlecampus", "already logged in");

            } else {

                utils.connection.login(name, password,"");

                // See if you are authenticated
                System.out.println(utils.connection.isAuthenticated());
                if (utils.connection.isAuthenticated()) {
                    Log.i("idlecampus", "logging in");

//				this.gotRoster();
                }


            }
        } catch (XMPPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void processPacket(Packet arg0) {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(currentActivity.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String savedMessages = appSharedPrefs.getString("Messages", "");
        JSONArray jsonSavedMessages = gson.fromJson(savedMessages, JSONArray.class);
        if (jsonSavedMessages == null)
            jsonSavedMessages = new JSONArray();
        Context context = currentActivity.getApplicationContext();
        Intent notificationIntent = new Intent(context,
                GroupMessagesActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
//                notificationIntent, 0);


        // TODO Auto-generated method stub
        System.out.println("xmpppacket " + arg0.toXML());
        StringReader sr = new StringReader(arg0.toXML());
        InputSource is = new InputSource(sr);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();


        try {

            DocumentBuilder builder = dbf.newDocumentBuilder();


            Document doc = builder.parse(is);
            doc.getDocumentElement().normalize();
            System.out.println("Root element of the doc is " +
                    doc.getDocumentElement().getNodeName());
            NodeList listOfPersons = doc.getElementsByTagName("item");
            System.out.println("listofpersons" +
                    listOfPersons.getLength());
            Node item = ((Node) listOfPersons.item(0));
            System.out.println("textFNList" +
                    item);

            NodeList textFNList = item.getChildNodes();
            String message =
                    ((Node) textFNList.item(0)).getNodeValue().trim();


            String message_to_send = message.substring(4, message.length() - 4);
            
            jsonSavedMessages.put(message_to_send);


//        String json = appSharedPrefs.getString("MyObject", "");
//        JSONObject obj = gson.fromJson(json, JSONObject.class);

//            JSONObject jsonObject = new JSONObject(page);
            // Save the JSONOvject

            String json = gson.toJson(jsonSavedMessages);
            prefsEditor.putString("Messages", json);
            prefsEditor.commit();
            System.out.println(json);


            String contentTitle = message_to_send;
            String contentText = message_to_send;

            int icon = 0;// = R.drawable.ic_stat_gcm;
            long when = System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new Notification(R.drawable.ic_launcher, message_to_send, when);
//            notification.vibrate = new long[] { 100, 250, 100, 500};
            String title = context.getString(R.string.app_name);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notification.sound = alarmSound;
            notificationIntent.putExtra("messageReceived", message_to_send);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent =
                    PendingIntent.getActivity(context, 0, notificationIntent, 0);
            notification.setLatestEventInfo(context, title, message_to_send, intent);
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
//            notificationManager.notify(0, notification);

            //---send a broadcast to inform the activity
// that the file has been downloaded---
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("Message Received");
            broadcastIntent.putExtra("message", message_to_send);
            currentActivity.getBaseContext().sendBroadcast(broadcastIntent);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
//            c.error(e.getMessage());
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
//            c.error(e.getMessage());
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
//            c.error(e.getMessage());
        }


    }


    @Override
    public void processMessage(Chat arg0, Message arg1) {
        // TODO Auto-generated method stub
        System.out.println("xmppchat " + arg1);

    }


    private class DownloadWebPageTask extends AsyncTask<Object, Void, Void> {

        private ProgressDialog waitingDialog;
        private String status;
        @Override
        protected Void doInBackground(Object... urls) {
            String response = "";
            SharedPreferences settings = currentActivity.getApplicationContext().getSharedPreferences("XMPP", 0);
            Map<String, String> params = new HashMap<String, String>();
            SharedPreferences.Editor editor = settings.edit();

            XMPPUtilities utils = (XMPPUtilities) urls[0];
            String name = (String) urls[1];
            String password = (String) urls[2];
            String group = (String) urls[4];
            status = (String) urls[5];
            String email = (String) urls[3];
            Log.i("idlecampus", "" + status);

            Log.i("idlecampus", "" + name);

            Log.i("idlecampus", "" + password);

            Log.i("idlecampus", "" + utils);


            Log.i("idlecampus", "" + utils.connection.isConnected());
            AccountManager am = new AccountManager(utils.connection);
            String device_identifier = settings.getString("device_identifier", "");
            params.put("device_identifier", device_identifier);
            params.put("jabber_id", name + "@idlecampus.com");
            params.put("email", email);
            params.put("name", name);
            params.put("password", password);

            if (status.equalsIgnoreCase("register")) {

                try {

//                    utils.connection.connect();

                    utils.connection.connect();
                    am.createAccount(name.trim(), password);




                    editor.putString("name", name);
                    editor.putString("password", password);
                    editor.putString("email", email);
                    // Commit the edits!
                    editor.commit();


                    try {
                        Log.i("idlecampus", "sending");
                        ServerUtilities.post("http://idlecampus.com/api/users", params);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        Log.i("idlecampus", "register");
                        e.printStackTrace();
                    }
                    Log.i("", "inside register");

                    System.out.println(utils.connection.isConnected());


                } catch (XMPPException e) {


                    e.printStackTrace();
                }
            }


            try {


                Log.i("idlecampus", "inside login");


                if (!utils.connection.isConnected()) {
                    utils.connection.connect();
                    Log.i("idlecampus", "packet listener");
                    utils.connection.addPacketListener(XMPP.this, null);
                }

                if (utils.connection.isAuthenticated()) {


                    Log.i("idlecampus", "already logged in");
//                    temp.get("login").success();
                } else {
                    editor.putString("name", name);
                    editor.putString("password", password);

                    // Commit the edits!
                    editor.commit();

                    try {
                        Log.i("idlecampus", "loggin and senging");
                        ServerUtilities.post("http://idlecampus.com/users/login", params);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        Log.i("idlecampus", "register");
                        e.printStackTrace();
                    }


                    utils.connection.login(name, password, "");

                    // See if you are authenticated
                    System.out.println(utils.connection.isAuthenticated());
                    if (utils.connection.isAuthenticated()) {
                        Log.i("idlecampus", "logging in");

                        if (status.equalsIgnoreCase("register")) {
                            Log.i("idlecampus", "joining group");

                            JSONObject jsonObject = getGroupName(group);
                            if (jsonObject != null) {
                                XMPP.subscribeNode(group);
                                editor.putString("code", "2");
                                try {
                                    editor.putString("group_code", jsonObject.getString("group_code"));
                                    editor.putString("group_name", jsonObject.getString("group_name"));
                                    editor.commit();
                                } catch (JSONException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }

                                try {
                                    getTimetable(group);

                                    showGroup(utils, jsonObject.getString("group_name"));
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            } else {
                                Context context = currentActivity.getApplicationContext();
                                CharSequence text = "Group not found.!";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }


                        } else if (status.equalsIgnoreCase("login")||status.equalsIgnoreCase("autologin")) {
                            //return here with a status of 1.

                            //get group name and then show timetable for the group.

//                            gotRoster();

                            try {

                                List<Subscription> subscriptions = utils.mgr.getSubscriptions();
                                for (int i = 0; i < subscriptions.size(); i++) {
                                    Log.i("idlecampussubscriptions", subscriptions.get(i).getNode());
                                }


                                // Get the affiliations for the users bare JID
                                List<Affiliation> affiliations = utils.mgr.getAffiliations();

                                for (int i = 0; i < affiliations.size(); i++) {
                                    Log.i("idlecampusaffiliations", affiliations.get(i).getNode());
                                }

                                if (subscriptions.size() > 0) {
                                    JSONObject jsonObject = getGroupName(subscriptions.get(0).getNode());
                                    try {
                                        editor.putString("code", "2");
                                        editor.putString("group_code", jsonObject.getString("group_code"));
                                        editor.putString("group_name", jsonObject.getString("group_name"));
                                        editor.commit();
                                        getTimetable(jsonObject.getString("group_code"));
                                        if (!isResumed) {
                                        showGroup(utils, jsonObject.getString("group_name"));
                                        }
                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                } else if (affiliations.size() > 0) {
                                    try {

                                        JSONObject jsonObject = getGroupName(affiliations.get(0).getNode());
                                        editor.putString("code", "1");
                                        editor.putString("group_code", jsonObject.getString("group_code"));
                                        editor.putString("group_name", jsonObject.getString("name"));
                                        editor.commit();
                                        Intent i = new Intent(currentActivity.getBaseContext(), NewMessage.class);
                                        i.putExtra("group", jsonObject.getString("group_name"));

                                        currentActivity.startActivity(i);
                                        currentActivity.finish();
                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                }

                            } catch (XMPPException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }


                            Log.i("idlecampus", "packet listener");
                            utils.connection.addPacketListener(XMPP.this, null);
//                            temp.get("login1").success("1");
                        } else
                            Log.i("idlecampus", "packet listener");
                        utils.connection.addPacketListener(XMPP.this, null);
                        Log.i("idlecampus", "sending back login1 with status 2");
//                        temp.get("login").success("2");


                    }


                }


            } catch (XMPPException e) {
                XMPPUtilities.xmppUtils = null;
                editor.putString("name", "");
                editor.putString("password", "");
                editor.commit();
                currentActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Context context = currentActivity.getApplicationContext();
                        CharSequence text = "Invalid Credentials.!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                });

                e.printStackTrace();
            }


            return null;
        }

        public JSONObject getGroupName(String group) {
            String page = null;

            JSONObject object = new JSONObject();
            DefaultHttpClient httpClient = new DefaultHttpClient();

            HttpGet httpGet = new HttpGet("http://idlecampus.com/api/groups/" + group);
            // httpGet.addHeader("Content-type","application/json");


            ResponseHandler<String> resHandler = new BasicResponseHandler();

            try {
                page = httpClient.execute(httpGet, resHandler);
            } catch (ClientProtocolException e) {

                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block

            }
            Log.e("response", page);
            if (page.equalsIgnoreCase("group not found")) {
                return null;
            } else {
                try {


                    JSONObject jsonObject = new JSONObject(page);


                    try {
                        object.put("group_name", jsonObject.getString("name"));
                        object.put("group_code", jsonObject.getString("group_code"));
//                object.put("code", "2");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    System.out.println(object);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return object;

        }

        private void showGroup(XMPPUtilities utils, String group) {
            Log.i("idlecampus", "packet listener");
            utils.connection.addPacketListener(XMPP.this, null);
            Intent i = new Intent(currentActivity.getBaseContext(), MyGroup.class);
            i.putExtra("group", group);

            currentActivity.startActivity(i);
            currentActivity.finish();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            final Activity t = cordova.getActivity();
            if(!status.equalsIgnoreCase("autologin"))
            waitingDialog = ProgressDialog.show(currentActivity, "", "Initializing...", true);
            Log.d("WaitingDialog", "Initializing...");
            Log.d("WaitingDialog", "Dialog shown, waiting hide command");


        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(!status.equalsIgnoreCase("autologin"))

            try{
                waitingDialog.dismiss();
            }
            catch(Exception e){
                Toast.makeText(currentActivity, "IOE response " + e.toString(), Toast.LENGTH_LONG).show();
            }
        }

        public void getTimetable(String group) {
            SharedPreferences settings = currentActivity.getApplicationContext().getSharedPreferences("XMPP", 0);

            SharedPreferences.Editor editor = settings.edit();
            String timetableJSON1 = settings.getString("data","");
           String timetableJSON = settings.getString("timetable","");

            if(timetableJSON.equalsIgnoreCase(""))  {

            Timetable timetable = new Timetable();
            timetable.currentActivity = currentActivity;
            timetable.notification("http://idlecampus.com/groups/"+group+"/timetable.json");
            }


        }
    }

    public void registerandlogin(Activity act, XMPPUtilities utils, String name, String password, String email, String group, String status) {
        currentActivity = act;
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.status = status;
        task.execute(new Object[]{utils, name, password, email, group, status});

    }


}

