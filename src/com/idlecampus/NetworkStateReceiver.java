package com.idlecampus;

import org.jivesoftware.smack.XMPPException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 4/7/13
 * Time: 11:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class NetworkStateReceiver extends BroadcastReceiver {
    XMPPUtilities utils = null;
    Context ctx = null;
    public void onReceive(Context context, Intent intent) {
        ctx = context;

        utils = XMPPUtilities.getInstance();
        Log.d("app","Network connectivity change");
        if(intent.getExtras()!=null) {
            NetworkInfo ni=(NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
            if(ni!=null && ni.getState()==NetworkInfo.State.CONNECTED) {
                Log.i("app","Network "+ni.getTypeName()+" connected");

                //login here
                utils = XMPPUtilities.getInstance();
                SharedPreferences settings = context.getSharedPreferences("XMPP", 0);
                String name = settings.getString("name", "");
                String password = settings.getString("password","");
                Log.i("idlecampus","onresume");
                String group = settings.getString("group_name", "");
               if(!name.equalsIgnoreCase("") && !password.equalsIgnoreCase("") && !utils.connection.isConnected()){
                   try {
					utils.connection.connect();
				} catch (XMPPException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//            finish();
                }
            }
        }
        if(intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
            Log.d("app","There's no network connectivity");
            if (utils.connection.isConnected()) {


                Log.i("idlecampus offline", "already logged in");

                utils.connection.disconnect();
            }

        }
    }
    public void login(String name,String password, String status){
        XMPP xmpp = new XMPP();

        System.out.println(utils.connection.isConnected());

        if (utils.connection.isAuthenticated()) {


            Log.i("idlecampus", "already logged in");

        } else {

//            xmpp.registerandlogin(null,ctx, utils, name, password, "", "", status);
        }

    }
}
