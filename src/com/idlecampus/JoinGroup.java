package com.idlecampus;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.PubSubManager;
import android.content.Intent;
/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 26/6/13
 * Time: 6:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class JoinGroup extends Activity {
    private String name = null;
    private String email = null;
    private String password = null;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the message from the intent
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        email = intent.getStringExtra("email");
        password = intent.getStringExtra("password");
        setContentView(R.layout.activity_join_group_screen);
    }
    public void joinGroup(View view){
        final XMPPUtilities utils = XMPPUtilities.getInstance();
        final  AccountManager am = new AccountManager(utils.connection);
        EditText editTextName = (EditText) findViewById(R.id.join_group_edit_text);
        String node = editTextName.getText().toString().toUpperCase();
        XMPP xmpp = new XMPP();

        System.out.println(utils.connection.isConnected());
        xmpp.registerandlogin(this,utils,name,password,email,node,"register");
    }
}