package com.idlecampus;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 28/6/13
 * Time: 11:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class GroupMessagesActivity extends Activity implements AdapterView.OnItemClickListener {
    GroupMessagesAdapter groupMessagesAdapter = null;
    public static ListView listView = null;
    List<String> messages = null;
    IntentFilter intentFilter;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);
        messages = new ArrayList<String>();

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String savedMessages = appSharedPrefs.getString("Messages", "");
        JSONArray jsonSavedMessages = gson.fromJson(savedMessages, JSONArray.class);

        if (jsonSavedMessages == null)
            jsonSavedMessages = new JSONArray();
        for (int i=0; i<jsonSavedMessages.length(); i++) {
            try {
				messages.add( jsonSavedMessages.getString(i) );
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        setContentView(R.layout.activity_group_messages_screen);
        listView = (ListView) findViewById(R.id.groupMessagesList);
        groupMessagesAdapter = new GroupMessagesAdapter();

        GroupMessagesAdapter.messages = messages;
        listView.setAdapter(groupMessagesAdapter);



    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(intentReceiver);
    }

    public void clearMessages(View view){

        groupMessagesAdapter.messages.clear();

        groupMessagesAdapter.notifyDataSetChanged();


        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String savedMessages = appSharedPrefs.getString("Messages", "");
        JSONArray jsonSavedMessages = gson.fromJson(savedMessages, JSONArray.class);

        jsonSavedMessages = new JSONArray();




        String json = gson.toJson(jsonSavedMessages);
        prefsEditor.putString("Messages", json);
        prefsEditor.commit();
        System.out.println(json);
    }
    @Override
    public void onResume() {
        super.onResume();
        groupMessagesAdapter.notifyDataSetChanged();
        //---intent to filter for file downloaded intent---
        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);
    }



    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
        LinearLayout ll = (LinearLayout) view;
        TextView tv = (TextView) ll.findViewById(R.id.time_view);
        final String weekday = tv.getText().toString();

        Intent i = new Intent(getBaseContext(), MessageActivity.class);
        i.putExtra("weekday", weekday);
        // find out the string value and set the contents of the next page accordingly.
        startActivity(i);


    }

    //	@Override
//    public void processPacket(Packet arg0) {
//        Context context = getApplicationContext();
//        Intent notificationIntent = new Intent(context,
//                GroupMessagesActivity.class);
////        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
////                notificationIntent, 0);
//
//
//        // TODO Auto-generated method stub
//        System.out.println("xmpppacket " + arg0.toXML());
//        StringReader sr = new StringReader(arg0.toXML());
//        InputSource is = new InputSource(sr);
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//
//
//        try {
//
//            DocumentBuilder builder = dbf.newDocumentBuilder();
//
//
//            Document doc = builder.parse(is);
//            doc.getDocumentElement().normalize();
//            System.out.println("Root element of the doc is " +
//                    doc.getDocumentElement().getNodeName());
//            NodeList listOfPersons = doc.getElementsByTagName("item");
//            System.out.println("listofpersons" +
//                    listOfPersons.getLength());
//            Node item = ((Node) listOfPersons.item(0));
//            System.out.println("textFNList" +
//                    item);
//
//            NodeList textFNList = item.getChildNodes();
//            String message =
//                    ((Node) textFNList.item(0)).getNodeValue().trim();
//
//
//            String message_to_send = message.substring(4, message.length() - 4);
//            messages.add(message_to_send);
//            groupMessagesAdapter.notifyDataSetChanged();
//
//        } catch (ParserConfigurationException e) {
//            Log.e("Error: ", e.getMessage());
////            c.error(e.getMessage());
//        } catch (SAXException e) {
//            Log.e("Error: ", e.getMessage());
////            c.error(e.getMessage());
//        } catch (IOException e) {
//            Log.e("Error: ", e.getMessage());
////            c.error(e.getMessage());
//        }
//
//
//    }
    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String message = intent.getStringExtra("message");
            int duration = Toast.LENGTH_SHORT;
            groupMessagesAdapter.messages.add(message);

            groupMessagesAdapter.notifyDataSetChanged();
//            Toast.makeText(getBaseContext(), message,duration).show();

        }
    };
}