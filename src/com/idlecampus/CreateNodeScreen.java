package com.idlecampus;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.pubsub.AccessModel;
import org.jivesoftware.smackx.pubsub.ConfigureForm;
import org.jivesoftware.smackx.pubsub.FormType;
import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.NodeType;
import org.jivesoftware.smackx.pubsub.PubSubManager;
import org.jivesoftware.smackx.pubsub.PublishModel;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class CreateNodeScreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_screen);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_create_node_screen, menu);
        return true;
    }
    
    public void createNode(View view){
    	EditText editTextName = (EditText) findViewById(R.id.editText1);
		String node = editTextName.getText().toString();
		//send a message to bot@ankurs-macbook-pro.local
		XMPPUtilities utils = XMPPUtilities.getInstance();
//		String message = "create" + node;
//		try {
//			utils.sendMessage(message, "bot@ankurs-macbook-pro.local");
//		} catch (XMPPException e) {
//			// TODO Auto-generated catch block
//			e.pr intStackTrace();
//		}
		PubSubManager mgr = utils.mgr;
		
		
		try {
//			leaf = (LeafNode) mgr.getNode(node);
//			leaf.subscribe(utils.connection.getUser());
			
			
	        LeafNode leaf ;
	        leaf = mgr.createNode(node);
//			ConfigureForm form = new ConfigureForm(FormType.submit);
//			form.setAccessModel(AccessModel.open);
//			form.setDeliverPayloads(true);
//			form.setNotifyRetract(true);
//			form.setPersistentItems(true);
//			form.setPublishModel(PublishModel.open);
//
//			leaf.sendConfigurationForm(form);  
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
    }
}
