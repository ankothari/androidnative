package com.idlecampus;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.XMPPException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.jivesoftware.smackx.pubsub.LeafNode;
import org.jivesoftware.smackx.pubsub.PubSubManager;
import com.idlecampus.XMPP;
public class RegisterScreen extends Activity {

     Handler handler = null;
	private ProgressDialog pd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        // creating connection detector class instance
        cd = new ConnectionDetector(getApplicationContext());


		setContentView(R.layout.activity_register_screen);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_register_screen, menu);
		return true;
	}



    public void register(View view) {

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            EditText editTextName = (EditText) findViewById(R.id.registerusername);
            final  String name = editTextName.getText().toString();
            if (name.matches("")) {
                Toast.makeText(this, "You did not enter a name", Toast.LENGTH_SHORT).show();
                return;
            }
            EditText editTextEmail = (EditText) findViewById(R.id.registeremail);
            final String email = editTextEmail.getText().toString();
            if (email.matches("")) {
                Toast.makeText(this, "You did not enter a email", Toast.LENGTH_SHORT).show();
                return;
            }
            EditText editTextPassword = (EditText) findViewById(R.id.registerpassword);
            final String password = editTextPassword.getText().toString();
            if (password.matches("")) {
                Toast.makeText(this, "You did not enter a password", Toast.LENGTH_SHORT).show();
                return;
            }
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    // get the bundle and extract data by key
                    Bundle b = msg.getData();
                    boolean isEverythingAlright = b.getBoolean("My Key");
                    String whatisnotavailable = b.getString("whatisnotavailable");
//                    pd.dismiss();
                    if(isEverythingAlright){

                        Intent i = new Intent(getBaseContext(), JoinGroup.class);
                        i.putExtra("name", name);
                        i.putExtra("email",email);
                        i.putExtra("password",password);
                        startActivity(i);
                        finish();


                    }
                    else{
                        Context context = getApplicationContext();
                        CharSequence text = whatisnotavailable+" already in use.!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }


                }
            };
            // create a new thread
            Thread background = new Thread(new Runnable() {

                @Override
                public void run() {

                    checkNameAndEmail(name,email);



                }

            });
//            pd = ProgressDialog.show(this, "Checking..", "Checking Availability", true,       false);
            background.start();
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }













    }

    public void checkNameAndEmail(String name,String email){


        String response = "";






        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://www.idlecampus.com/checkName?name="+name.trim());

        // httpGet.addHeader("Content-type","application/json");


        ResponseHandler<String> resHandler = new BasicResponseHandler();

        try
        {
            response = httpClient.execute(httpGet, resHandler);
            if(Integer.parseInt(response)==0){
                checkEmail(email);
            }

            else{

                Message msg = new Message();
                Bundle b = new Bundle();
                String res1 = null;
                b.putBoolean("My Key", Boolean.valueOf(false));
                b.putString("whatisnotavailable", "Name");
                msg.setData(b);
                handler.sendMessage(msg);

            }
        }
        catch (ClientProtocolException e)
        {

            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block

        }
        Log.e("responcename",response);

    }

    public void checkEmail(String email){
        String response = "";






        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://www.idlecampus.com/checkEmail?email="+email);

        // httpGet.addHeader("Content-type","application/json");


        ResponseHandler<String> resHandler = new BasicResponseHandler();

        try
        {
            response = httpClient.execute(httpGet, resHandler);
            if(Integer.parseInt(response)==0){
                Message msg = new Message();
                Bundle b = new Bundle();
                String res1 = null;
                b.putBoolean("My Key", Boolean.valueOf(true));
                msg.setData(b);
                handler.sendMessage(msg);
            }
            else{
                Message msg = new Message();
                Bundle b = new Bundle();
                String res1 = null;
                b.putBoolean("My Key", Boolean.valueOf(false));
                b.putString("whatisnotavailable", "Email");
                msg.setData(b);
                handler.sendMessage(msg);
            }
        }
        catch (ClientProtocolException e)
        {

            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block

        }
        Log.e("responseemail",response);
    }





    private class DownloadWebPageTask extends AsyncTask<Object, Void, String> {

        private ProgressDialog waitingDialog;
        private String whattocheck = null;
        @Override
        protected String doInBackground(Object... urls) {
            String response = "";


            String url = (String) urls[0];
             whattocheck = (String) urls[1];



            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);

            // httpGet.addHeader("Content-type","application/json");


            ResponseHandler<String> resHandler = new BasicResponseHandler();

            try
            {
                response = httpClient.execute(httpGet, resHandler);
            }
            catch (ClientProtocolException e)
            {

                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block

            }
            Log.e("response",response);



            return response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            final Activity t = getParent();

            waitingDialog = ProgressDialog.show(t, "", "Checking "+whattocheck, true);
//            LOG.d("WaitingDialog", "Initializing...");
//            LOG.d("WaitingDialog", "Dialog shown, waiting hide command");


        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            int res = Integer.parseInt(result);

            if(res == 0 && whattocheck.equalsIgnoreCase("name")){
//                this.execute(new Object[]{"email",url});
            }
            waitingDialog.dismiss();
        }
    }


    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
//        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }




}
