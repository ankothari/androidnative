//package com.idlecampus;
//
//import java.util.Iterator;
//
//import org.jivesoftware.smack.XMPPException;
//import org.jivesoftware.smackx.ServiceDiscoveryManager;
//import org.jivesoftware.smackx.packet.DiscoverItems;
//
//import android.os.Bundle;
//import android.app.Activity;
//import android.content.Intent;
//import android.view.Menu;
//import android.view.View;
//import android.widget.EditText;
//
//public class LoginScreen extends Activity {
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_login_screen);
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.activity_login_screen, menu);
//		return true;
//	}
//
//	public void login(View view) {
//		EditText editTextName = (EditText) findViewById(R.id.editText1);
//		String name = editTextName.getText().toString();
//
//		EditText editTextPassword = (EditText) findViewById(R.id.editText2);
//		String password = editTextPassword.getText().toString();
//
//		try {
//
//			XMPPUtilities utils = XMPPUtilities.getInstance();
//			utils.connection.login(name, password);
//
//			// See if you are authenticated
//			System.out.println(utils.connection.isAuthenticated());
//
//			if(utils.connection.isAuthenticated()){
//				Intent intent = new Intent(this, ConfigureStreamsScreen.class);
//				startActivity(intent);
//			}
//		} catch (XMPPException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//}
