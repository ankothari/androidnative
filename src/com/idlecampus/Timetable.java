package com.idlecampus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 27/6/13
 * Time: 10:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class Timetable {
    public Activity currentActivity = null;
public static HashMap<String,  List<String>> timetableMap=new HashMap<String,  List<String>>();
    public void notification(String url)
        {

            System.out.println(url);
            String page = null;

            JSONObject object = new JSONObject();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);


            ResponseHandler<String> resHandler = new BasicResponseHandler();

            try {
                page = httpClient.execute(httpGet, resHandler);
            } catch (ClientProtocolException e) {

                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
                // TODO Auto-generated catch block

            }
            Log.e("response", page);
            try {

                SharedPreferences settings = currentActivity.getApplicationContext().getSharedPreferences("XMPP", 0);
                Editor prefsEditor = settings.edit();
                JSONObject jsonObject = new JSONObject(page);
                // Save the JSONOvject
                Gson gson = new Gson();
                String json = gson.toJson(jsonObject);
                prefsEditor.putString("data", json);
                prefsEditor.commit();
                System.out.println(json);
                try {

                    JSONObject timetable = jsonObject.getJSONObject("timetable");
                    JSONArray entries = timetable.getJSONArray("field_entries");

                    if (entries.length() == 0) {
                      return;
                    }
                   JSONArray weekdays = timetable.getJSONArray("weekdays");
                   String weekdaysJson = gson.toJson(weekdays);
                    prefsEditor.putString("weekdays", weekdaysJson);
                    prefsEditor.commit();
                    
                   
                    List<String> mondayList = new ArrayList<String>();
                    List<String> tuesdayList = new ArrayList<String>();
                    List<String> wednesdayList = new ArrayList<String>();
                    List<String> thursdayList = new ArrayList<String>();
                    List<String> fridayList = new ArrayList<String>();
                    List<String> saturdayList = new ArrayList<String>();
                    List<String> sundayList = new ArrayList<String>();
                    List<String> weekdaysList = new ArrayList<String>();

                    for(int len = 0;len < weekdays.length();len++){
                        weekdaysList.add(weekdays.getString(len));
                    }
                    
                  
                    for (int a = 0; a < entries.length(); a++) {

                        for (int b = 0; b < entries.getJSONArray(a).length(); b++) {

                            for (int c = 0; c < entries.getJSONArray(a).getJSONArray(b).length(); c++) {

                                JSONObject obj = entries.getJSONArray(a).getJSONArray(b).getJSONObject(c);
                                System.out.println(entries.getJSONArray(a).getJSONArray(b).getJSONObject(c));
                                String str = "";
                                str = str + getDisplayTime(entries.getJSONArray(a)) + '\n';


                                Iterator iter = obj.keys();
                                while (iter.hasNext()) {
                                    String key = (String) iter.next();
                                    String value = obj.getString(key);


                                    if (value != null && !value.equalsIgnoreCase("null")) {
                                        if (!key.equalsIgnoreCase("from_hours") && !key.equalsIgnoreCase("from_minutes") && !key.equalsIgnoreCase("to_minutes") && !key.equalsIgnoreCase("to_hours") && !key.equalsIgnoreCase("weekday"))
                                            str += key + ':' + value + '\n';
                                    }
                                    System.out.println(str);
                                }

                                if (obj.getString("weekday").equalsIgnoreCase("Monday")) {
                                	mondayList.add(str);
                                    
                                }
                              
                                if (obj.getString("weekday").equalsIgnoreCase("Tuesday")) {
                                	tuesdayList.add(str);
                                }

                                if (obj.getString("weekday").equalsIgnoreCase("Wednesday")) {
                                	wednesdayList.add(str);
                                }
                                if (obj.getString("weekday").equalsIgnoreCase("Thursday")) {
                                	thursdayList.add(str);
                                }
                                if (obj.getString("weekday").equalsIgnoreCase("Friday")) {
                                	fridayList.add(str);
                                }
                                if (obj.getString("weekday").equalsIgnoreCase("Sunday")) {
                                	sundayList.add(str);
                                }
                                if (obj.getString("weekday").equalsIgnoreCase("Saturday")) {
                                	saturdayList.add(str);
                                }

                            }
                        }
                    }


                    timetableMap.put("Monday",mondayList);
                    timetableMap.put("Tuesday",tuesdayList);
                    timetableMap.put("Wednesday",wednesdayList);
                    timetableMap.put("Thursday",thursdayList);
                    timetableMap.put("Friday",fridayList);
                    timetableMap.put("Saturday",saturdayList);
                    timetableMap.put("Sunday",sundayList);
                    timetableMap.put("weekdays",weekdaysList);


                    MapWrapper wrapper = new MapWrapper();
                    wrapper.setMyMap(timetableMap);
                    String serializedMap = gson.toJson(wrapper);

                    prefsEditor.putString("timetable", serializedMap);
                    prefsEditor.commit();





                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println(object);


            } catch (Exception e) {
                e.printStackTrace();

            }
        }



    public String getDisplayTime(JSONArray time1) {
    	  String display_from = null, display_to = null, from_date_hour = null, from_date_minute = null, to_date_hour = null, to_date_minute = null;
    	    
    	    if (time1.length() > 0) {
    	        try {
					from_date_hour = time1.getJSONArray(0).getJSONObject(0).getString("from_hours");
					 from_date_minute = time1.getJSONArray(0).getJSONObject(0).getString("from_minutes");
		    	        to_date_minute = time1.getJSONArray(0).getJSONObject(0).getString("to_minutes");
		    	        to_date_hour =time1.getJSONArray(0).getJSONObject(0).getString("to_hours");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    	       
    	        
    	        if (from_date_hour.equalsIgnoreCase("12") && from_date_minute.equalsIgnoreCase("0")) {
    	            
    	            display_from = "12 NOON";
    	        } else if (Integer.parseInt(from_date_hour) >= 12) {
    	            display_from = Integer.parseInt(from_date_hour) - 12 + ":";
    	            display_from = display_from + from_date_minute;
    	            display_from = display_from + "PM";
    	        } else {
    	            display_from = from_date_hour + ":";
    	            display_from = display_from + from_date_minute;
    	            display_from = display_from + "AM";
    	        }
    	        
    	        if (to_date_hour.equalsIgnoreCase("12") && to_date_minute.equalsIgnoreCase("0")) {
    	            
    	            display_to = "12 NOON";
    	        } else if (Integer.parseInt(to_date_hour) >= 12) {
    	            display_to = Integer.parseInt(to_date_hour) - 12 + ":";
    	            display_to = display_to + to_date_minute;
    	            display_to = display_to + "PM";
    	        } else {
    	            display_to = to_date_hour + ":";
    	            display_to = display_to + to_date_minute;
    	            display_to = display_to + "AM";
    	        }
    	        
    	        return display_from + "-" + display_to;
    	    }
			return display_from + "-" + display_to;



    }
    

}
