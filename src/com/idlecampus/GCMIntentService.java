/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.idlecampus;

import static com.idlecampus.CommonUtilities.SENDER_ID;
import static com.idlecampus.CommonUtilities.displayMessage;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

    @SuppressWarnings("hiding")
    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(SENDER_ID);
    }

      @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        displayMessage(context, getString(R.string.gcm_registered));
        ServerUtilities.register(context, registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
    	final Intent notificationIntent = new Intent(context, HomeScreen.class); notificationIntent.setAction(Intent.ACTION_MAIN);
    	notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        String app = intent.getExtras().getString("app");
    	 Log.i("idlecampus", "Received message:"+intent.getExtras().getString("app"));
    	 Log.i("idlecampus", "Received message:"+intent.getExtras().getString("url"));
    	 
    	 String url = intent.getExtras().getString("url");
    	 
         displayMessage(context, app);
        
         // notifies user 
         generateNotification(context, url,app);
    	 
//    	 MyApplication.getInstance().g.runOnUiThread(new Runnable() {
// 	        public void run() {
//
// 	        	String method = "timetable.notification(\""+url+"\")";
// 	        	Log.i("idlecampus",method);
//	        	 MyApplication.getInstance().g.loadUrl("javascript:"+method);
//	        }
//	    });
       
    }
  
    @Override 
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
//        generateNotification(context, message);
    }

    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message,String app) {
        int icon = 0;// = R.drawable.ic_stat_gcm;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(R.drawable.icon, "You have a new "+app, when);
        notification.vibrate = new long[] { 100, 250, 100, 500};  
        String title = context.getString(R.string.app_name);
       
        Intent notificationIntent = null;
//        notificationIntent = new Intent(context, HomeScreen.class)  ;
        notificationIntent = new Intent(context, SplashScreen.class);
//        if(app.equalsIgnoreCase("timetable"))
//        notificationIntent = new Intent(context, MyGroup.class);
//        else if(app.equalsIgnoreCase("message")){
//        notificationIntent = new Intent(context, HomeScreen.class);
//        }
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notification.sound = alarmSound;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }

}
