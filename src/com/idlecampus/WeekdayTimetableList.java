package com.idlecampus;

import java.util.HashMap;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.*;
import android.media.RingtoneManager;
import android.net.Uri;
import org.json.JSONObject;

import com.google.gson.Gson;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 28/6/13
 * Time: 1:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class WeekdayTimetableList extends Activity {
    IntentFilter intentFilter;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //---intent to filter for file downloaded intent---
        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);
        setContentView(R.layout.activity_weekday_timetable_screen);
        Intent intent = getIntent();
        String weekday = intent.getStringExtra("weekday");
        TextView weekdayTextView = (TextView)findViewById(R.id.weekdayDetailHeader);
        weekdayTextView.setText(weekday.toUpperCase());
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("XMPP", 0);
        String wrapperStr = preferences.getString("timetable","");
        Gson gson = new Gson();
        MapWrapper wrapper = gson.fromJson(wrapperStr, MapWrapper.class);
        HashMap<String, List<String>> timetableMap = wrapper.getMyMap();
        List<String> weekdayList = timetableMap.get(weekday);
        ListView listView = (ListView) findViewById(R.id.weekdayDetail);
        WeekdayInformationAdapter   weekdayInformationAdapter = new WeekdayInformationAdapter();
        weekdayInformationAdapter.timetable = weekdayList;
        listView.setAdapter(weekdayInformationAdapter);



        
    }
    @Override
    public void onResume() {
        super.onResume();

        //---intent to filter for file downloaded intent---
        intentFilter = new IntentFilter();
        intentFilter.addAction("Message Received");
        //---register the receiver---
        registerReceiver(intentReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(intentReceiver);
    }

    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {



            String message_to_send = intent.getStringExtra("message");


            String contentTitle = message_to_send;
            String contentText = message_to_send;

            int icon = 0;// = R.drawable.ic_stat_gcm;
            long when = System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new Notification(R.drawable.icon, message_to_send, when);
//            notification.vibrate = new long[] { 100, 250, 100, 500};
            String title = context.getString(R.string.app_name);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notification.sound = alarmSound;
            Intent notificationIntent = new Intent(context,
                    GroupMessagesActivity.class);
            notificationIntent.putExtra("messageReceived", message_to_send);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pIntent =
                    PendingIntent.getActivity(context, 0, notificationIntent, 0);
            notification.setLatestEventInfo(context, title, message_to_send, pIntent);
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, notification);

        }
    };
}