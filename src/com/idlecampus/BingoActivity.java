package com.idlecampus;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.Serializable;
import java.util.HashMap;

import android.widget.TextView;
import com.google.gson.Gson;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 16/7/13
 * Time: 5:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class BingoActivity extends Activity {
	Bingo bingo;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String user = intent.getStringExtra("userPlayingBingoWith");




        RelativeLayout layout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        ImageView topBlueBar= new ImageView(this);
        topBlueBar.setImageDrawable(getResources().getDrawable(R.drawable.topbluebarwithtext40));
        topBlueBar.setId(1);

        ImageView orangeBar=new ImageView(this);
        orangeBar.setImageDrawable(getResources().getDrawable(R.drawable.orangeheader));
        params2.addRule(RelativeLayout.BELOW, topBlueBar.getId());
        orangeBar.setId(2);

        bingo = new Bingo(this,user);
        bingo.setBackgroundColor(Color.WHITE);
        params3.addRule(RelativeLayout.BELOW, orangeBar.getId());
        bingo.setId(3);


        Button btn = new Button(this);
        params4.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, bingo.getId());
        btn.setText("New Game");
        btn.setId(4);


        layout.addView(topBlueBar, params1);
        layout.addView(orangeBar, params2);
        layout.addView(bingo, params3);
        layout.addView(btn, params4);

        setContentView(layout);

//        setContentView(bingo);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        HashMap<String, Serializable> userPlayingBingoWithMap=new HashMap<String, Serializable>();
        SharedPreferences settings = getSharedPreferences("XMPP", 0);
        SharedPreferences.Editor editor = settings.edit();

        userPlayingBingoWithMap.put("lines",bingo.bingoLines);
        userPlayingBingoWithMap.put("turn",bingo.myTurn);
        userPlayingBingoWithMap.put("myScore",bingo.myScore);
        userPlayingBingoWithMap.put("yourScore",bingo.yourScore);
        String json = bingo.gson.toJson(userPlayingBingoWithMap);
        editor.putString("userPlayingBingoWith", json);
        editor.commit();

    }
}