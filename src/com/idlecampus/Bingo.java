package com.idlecampus;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.jivesoftware.smack.XMPPException;

import com.google.gson.Gson;
import com.idlecampus.BingoKey;

public class Bingo extends View {
    ArrayList<Integer> numbers;
    Canvas canvas;
    int[][] points = new int[600][600];
    BingoKey[][] lines = new BingoKey[600][600];
    Random randomGenerator = new Random();
    boolean myTurn;
    int countX, countY, height, width;
    int MAGIC = 0;
    boolean ok = false;
    ArrayList<BingoKey> bingoLines;
    Context mContext;
    Gson gson;

    public Bingo(Context context, String user) {
        super(context);
        gson = new Gson();
        mContext = context;
        userPlayingBingoWith = user;
        paint.setColor(Color.BLACK);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        height = display.getHeight();
        width = display.getWidth();
        MAGIC = 320 / 40;
        numbers = new ArrayList<Integer>();
        bingoLines = new ArrayList<BingoKey>();
        for (int x = 125; x <= 600; x += 100) {


            for (int y = 125; y <= 600; y += 100) {
                int z = randomGenerator.nextInt(25) + 1;

                int indexOfZ = numbers.indexOf(z);

                if (z == 0 || indexOfZ != -1) y -= 100;
                else {

                    numbers.add(z);


                    paint.setTextSize(50);

                    BingoKey bingoKey = new BingoKey();
                    bingoKey.key = z;
                    bingoKey.pressed = false;
                    bingoKey.xPos = countX;
                    bingoKey.yPos = countY;
                    bingoKey.x = x;
                    bingoKey.y = y;
                    bingoLines.add(bingoKey);
                    lines[countX][countY] = bingoKey;

                    countY++;

                    if (countY == 5) {
                        countX++;
                        countY = 0;


                    }

                    points[x][y] = z;
                }
            }
        }
        for (int a = 5; a < 10; a++) {


            for (int b = 0; b < 5; b++) {

                lines[a][b] = lines[b][a - 5];
            }
        }

        for (int a = 0; a < 5; a++) {
            lines[10][a] = lines[a][a];


        }


        int l = 4;
        for (int a = 0; a < 5; a++) {

            lines[11][a] = lines[a][l--];


        }
    }

    Paint paint = new Paint();
    private String userPlayingBingoWith;
    public int myScore, yourScore;


    @Override
    public void onDraw(Canvas canvas) {
        SharedPreferences settings = mContext.getSharedPreferences("XMPP", 0);
        SharedPreferences.Editor editor = settings.edit();
        String json = settings.getString(userPlayingBingoWith, "");
        Gson gson = new Gson();
        HashMap<String, Serializable> userPlayingBingoWithMap = new HashMap<String, Serializable>();
        HashMap<String, Serializable> obj = gson.fromJson(json, userPlayingBingoWithMap.getClass());


        countX = 0;
        countY = 0;
        myTurn = true;

        for (int i = 0; i < bingoLines.size(); i++) {
            BingoKey key = bingoLines.get(i);
            int x = key.x;
            int y = key.y;
            int z = key.key;
            boolean pressed = key.pressed;
            canvas.drawText(z + "", x, y + 25, paint);
            if (pressed) {
//                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(8);
                canvas.drawLine(x - 25, y - 25, (float) (x + 75), (float) (y + 75), paint);
            }

        }

        paint.setStrokeWidth(3);
        for (int i = 1; i < 7; i++) {
            canvas.drawLine(100, 100 * i, 600, 100 * i, paint);
            canvas.drawLine(100 * i, 100, 100 * i, 600, paint);
        }

        paint.setStrokeWidth(3);
        canvas.drawText("B", 125, 750, paint);
        canvas.drawText("I", 225, 750, paint);
        canvas.drawText("N", 325, 750, paint);
        canvas.drawText("G", 425, 750, paint);
        canvas.drawText("O", 525, 750, paint);

        //        Bitmap tes = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), Bitmap.Config.RGB_565);

        boolean one = false, two = false, three = false, four = false, five = false;

        boolean isfirst = true;


//
//    // for every button clicked set the value of it to false...and then check how many lines have been cut total. if number of liens cut is 5 then BINGO.
        int lines_true = 0;
        for (int a = 0; a < 12; a++) {
            int count_of_true = 0;
            for (int b = 0; b < 5; b++) {
                BingoKey v = lines[a][b];

                if (v != null && v.pressed == true) {
                    count_of_true++;
//
                    if (count_of_true == 5) {
                        lines_true++;
                        Log.i("Bingo", "lines cut ho gayi" + lines_true);
                    }

                }
                if (lines_true == 5 && isfirst) {
                    isfirst = false;


                    XMPPUtilities utils = XMPPUtilities.getInstance();
                    try {
                        utils.sendMessage("lose", "debasis@idlecampus.com");
                    } catch (XMPPException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                }
            }
//
        }
        for (int a = 0; a < 12; a++) {
            int count_of_true = 0;
            for (int b = 0; b < 5; b++) {
                BingoKey v = lines[b][a];

                if (v != null && v.pressed == true) {
                    count_of_true++;
//
                    if (count_of_true == 5) {
                        lines_true++;
                        Log.i("Bingo", "lines cut ho gayi" + lines_true);
                    }

                }
                if (lines_true == 5 && isfirst) {
                    isfirst = false;


                    XMPPUtilities utils = XMPPUtilities.getInstance();
                    try {
                        this.youWin();
                        utils.sendMessage("lose", "debasis@idlecampus.com");
                    } catch (XMPPException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                }
            }
//
        }
        int count_of_true = 0;
        for (int equalPoints = 0; equalPoints < 5; equalPoints++) {


            BingoKey v = lines[equalPoints][equalPoints];
            if (v != null && v.pressed == true) {
                count_of_true++;
//
                if (count_of_true == 5) {
                    lines_true++;
                    Log.i("Bingo", "lines cut ho gayi" + lines_true);
                }

            }
            if (lines_true == 5 && isfirst) {
                isfirst = false;


                XMPPUtilities utils = XMPPUtilities.getInstance();
                try {
                    utils.sendMessage("lose", "debasis@idlecampus.com");
                } catch (XMPPException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }
        }
        count_of_true = 0;
        for (int equalPoints = 0; equalPoints < 5; equalPoints++) {


            BingoKey v = lines[equalPoints][4 - equalPoints];
            if (v != null && v.pressed == true) {
                count_of_true++;
//
                if (count_of_true == 5) {
                    lines_true++;
                    Log.i("Bingo", "lines cut ho gayi" + lines_true);
                }

            }
            if (lines_true == 5 && isfirst) {
                isfirst = false;


                XMPPUtilities utils = XMPPUtilities.getInstance();
                try {
                    utils.sendMessage("lose", "debasis@idlecampus.com");
                } catch (XMPPException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }
        }


        int a = 0;
        int b = 0;

        paint.setStrokeWidth(8);
        switch (lines_true) {
            case 5:
                a = 525;
                b = 750;
                canvas.drawLine(a - 25, b - 75, (float) (a + 75), b + 50, paint);
            case 4:
                a = 225;
                b = 750;
                canvas.drawLine(a - 25, b - 75, (float) (a + 75), b + 50, paint);
            case 3:
                a = 325;
                b = 750;
                canvas.drawLine(a - 25, b - 75, (float) (a + 75), b + 50, paint);
            case 2:
                a = 225;
                b = 750;
                canvas.drawLine(a - 25, b - 75, (float) (a + 75), b + 50, paint);
            case 1:
                a = 125;
                b = 750;

//            canvas.rotate(45, a - 8, b + 20);

                canvas.drawLine(a - 25, b - 75, (float) (a + 75), b + 50, paint);
                break;
        }
        userPlayingBingoWithMap = new HashMap();
        settings = mContext.getSharedPreferences("XMPP", 0);
        editor = settings.edit();

        userPlayingBingoWithMap.put("lines", bingoLines);
        userPlayingBingoWithMap.put("turn", myTurn);
        json = gson.toJson(userPlayingBingoWithMap);
        editor.putString("userPlayingBingoWith", json);
        editor.commit();

    }

    private void youWin() {
        // TODO Auto-generated method stub
        //do this in a thread

//    	        status.text = @"You Win";
//    	        int myscore = [myScore.text intValue];
//    	        myscore++;
//    	        myScore.text = [NSString stringWithFormat:@"%d",myscore];
//    	       
//    	        turn.text = @"";
        XMPPUtilities utils = XMPPUtilities.getInstance();
        try {
            utils.sendMessage("lose", userPlayingBingoWith);
        } catch (XMPPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public boolean onTouchEvent(MotionEvent event) {
        ok = true;
        invalidate();
        float offsetX = event.getX();
        float offsetY = event.getY();

        if (myTurn) {

//

//
            if (offsetX > 600 || offsetY > 600)
                return true;
            for (int x = (int) (Math.floor(offsetX)); x >= 125; x--) {
                boolean found = false;
//
                for (int y = (int) (Math.floor(offsetY)); y >= 125; y--) {
//
                    if (points[x][y] != 0) {
                        Log.i("Bingo", "" + x + "," + y + "," + points[x][y]);
//
                        draw(x, y);
                        sendData("BINGO", "a@a.com", "" + points[x][y]);
                        found = true;

//
                        break;

                    }

                }
                if (found)
                    break;


            }
        }


        return true;
    }

    private void sendData(String from_user, String to_user, String message) {
        XMPPUtilities utils = XMPPUtilities.getInstance();
        try {
            utils.sendMessage(message, to_user);
        } catch (XMPPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void draw(int x, int y) {
        for (int a = 0; a < 12; a++) {

            for (int b = 0; b < 5; b++) {
                BingoKey v = lines[a][b];
                if (v.key == points[x][y]) {
                    v.pressed = true;

                }

            }
        }
        this.invalidate();

    }

    private void draw1(int number) {

        for (int a1 = 0; a1 < 12; a1++) {

            for (int b1 = 0; b1 < 5; b1++) {
                BingoKey v = lines[a1][b1];
//
//            //here i have to find the coordinate corresponing the value.
                if (v.key == number) {
                    v.pressed = true;


                }
            }
        }
        this.invalidate();

    }
}
