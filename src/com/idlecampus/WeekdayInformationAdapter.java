package com.idlecampus;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: charlieanna
 * Date: 29/6/13
 * Time: 8:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class WeekdayInformationAdapter extends  BaseAdapter {

	public List<String> timetable;

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return timetable.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return timetable.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

    public View getView(int position, View view, ViewGroup parent) {
        if(view == null){
            LayoutInflater inflator = LayoutInflater.from(parent.getContext());
            view = inflator.inflate(R.layout.stream_list_item,parent,false);
        }
        String info = timetable.get(position);
        TextView textView = (TextView)view.findViewById(R.id.time_view);
        textView.setText(info);
        return view;
    }


//    @Override
//    public boolean isEnabled(int position) {
//        return false;
//    }
}
