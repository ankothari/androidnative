package com.idlecampus;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.pubsub.Affiliation;
import org.jivesoftware.smackx.pubsub.Subscription;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class HomeScreen extends Activity {
    MyService serviceBinder;
    Intent i;
    public XMPP xmpp = null;
    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;
    XMPPUtilities utils = XMPPUtilities.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("idlecampus", "oncreate");
        super.onCreate(savedInstanceState);

        cd = new ConnectionDetector(getApplicationContext());
        setContentView(R.layout.activity_home_screen);


    }

    public void forgotPassword(View view) {
        Intent intent = new Intent(this, ForgotPassword.class);
        startActivity(intent);
    }

    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public void login(View view) {

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        // check for Internet status
        if (isInternetPresent) {
            EditText editTextName = (EditText) findViewById(R.id.loginusername);
            String name = editTextName.getText().toString();
            if (name.matches("")) {
                Toast.makeText(this, "You did not enter a username", Toast.LENGTH_SHORT).show();
                return;
            }
            EditText editTextPassword = (EditText) findViewById(R.id.loginpassword);
            String password = editTextPassword.getText().toString();
            if (password.matches("")) {
                Toast.makeText(this, "You did not enter a password", Toast.LENGTH_SHORT).show();
                return;
            }
            login(name, password, "login");
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            showAlertDialog(this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }


    }

    public void login(String name, String password, String status) {
        XMPP xmpp = new XMPP();
        xmpp.isResumed = false;
        System.out.println(utils.connection.isConnected());

        if (utils.connection.isAuthenticated()) {


            Log.i("idlecampus", "already logged in");

        } else {

            xmpp.registerandlogin(this, utils, name, password, "", "", status);
        }

    }


    public void register(View view) {
        Intent intent = new Intent(this, RegisterScreen.class);
        startActivity(intent);
        finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_home_screen, menu);
//        return true;
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//
//    }

//    public void startService(View view) {
//        Intent intent = new Intent(getBaseContext(), MyService.class);
//        try {
//            URL[] urls = new URL[]{
//                    new URL("http://www.amazon.com/somefiles.pdf"),
//                    new URL("http://www.wrox.com/somefiles.pdf"),
//                    new URL("http://www.google.com/somefiles.pdf"),
//                    new URL("http://www.learn2develop.net/somefiles.pdf")};
//            intent.putExtra("URLs", urls);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        startService(intent);
//    }
//
//    public void stopService(View view) {
//        stopService(new Intent(getBaseContext(), MyService.class));
//    }
//
//    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(getBaseContext(), "File downloaded",
//                    Toast.LENGTH_LONG).show();
//        }
//
//    };


//    @Override
//    public void onResume() {
//        super.onResume();  // Always call the superclass method first
//        SharedPreferences settings = getSharedPreferences("XMPP", 0);
//        String name = settings.getString("name", "");
//        String password = settings.getString("password", "");
//        Log.i("idlecampus", "onresume");
//
//
//    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
//        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


}
